#include <fstream>
#include <iostream>
#include <stdint.h>
#include <string>
#include <vector>

#include "tinyxml2.h"

struct Info {
  uint16_t fontSize;
  uint8_t bitField;
  uint8_t charSet;
  uint16_t stretchH;
  uint8_t aa;
  uint8_t paddingUp;
  uint8_t paddingRight;
  uint8_t paddingDown;
  uint8_t paddingLeft;
  uint8_t spacingHoriz;
  uint8_t spacingVert;
  uint8_t outline;
};

struct Page {
  uint8_t id;
  std::string name;
};

struct Common {
  uint16_t lineHeight;
  uint16_t base;
  uint16_t scaleW;
  uint16_t scaleH;
  uint16_t pages;
  uint8_t bitField;
  uint8_t alphaChnl;
  uint8_t redChnl;
  uint8_t greenChnl;
  uint8_t blueChnl;
};

struct Char {
  uint32_t id;
  uint16_t x;
  uint16_t y;
  uint16_t width;
  uint16_t height;
  uint16_t xoffset;
  uint16_t yoffset;
  uint16_t xadvance;
  uint8_t page;
  uint8_t chnl;
};

const uint8_t BMF_VERSION = 3;
const uint8_t BLOCK_TYPE_INFO = 1;
const uint8_t BLOCK_TYPE_COMMON = 2;
const uint8_t BLOCK_TYPE_PAGES = 3;
const uint8_t BLOCK_TYPE_CHARS = 4;
const uint8_t BLOCK_TYPE_KERNIN_PAIRS = 5;

int main(int argc, const char **argv) {
  if (argc != 2) {
    std::cout << "Usage: " << argv[0] << " <file>" << std::endl;
  } else {
    std::string inputFileName = std::string(argv[1]);
    std::string outputFileName = inputFileName.substr(0, inputFileName.size() - 4) + ".fnt";

    std::cout << "Parsing: " << inputFileName << std::endl;
    std::cout << "into: " << outputFileName << std::endl;
    tinyxml2::XMLDocument input;
    input.LoadFile(inputFileName.c_str());

    std::fstream output(outputFileName.c_str(), std::ios::out | std::ios::binary);
    output.write("BMF", 3);
    output.write((char*)&BMF_VERSION, 1);

    tinyxml2::XMLElement *font = input.FirstChildElement("font");

    // Parse Info
    tinyxml2::XMLElement *infoElement = font->FirstChildElement("info");
    Info info = {};
    info.fontSize = infoElement->IntAttribute("fontSize");
    info.bitField = infoElement->IntAttribute("bitField");
    info.charSet = infoElement->IntAttribute("charSet");
    info.stretchH = infoElement->IntAttribute("stretchH", 100);
    info.aa = infoElement->IntAttribute("aa");
    info.paddingUp = infoElement->IntAttribute("paddingUp");
    info.paddingRight = infoElement->IntAttribute("paddingRight");
    info.paddingDown = infoElement->IntAttribute("paddingDown");
    info.paddingLeft = infoElement->IntAttribute("paddingLeft");
    info.spacingHoriz = infoElement->IntAttribute("spacingHoriz");
    info.spacingVert = infoElement->IntAttribute("spacingVert");
    info.outline = infoElement->IntAttribute("outline");
    std::string fontFace = infoElement->Attribute("face");

    // Parse Pages
    std::vector<Page> pages;
    tinyxml2::XMLElement *pagesElement = font->FirstChildElement("pages");
    tinyxml2::XMLElement *pageElement = pagesElement->FirstChildElement("page");
    while (pageElement) {
      Page page = {};
      page.id = pageElement->IntAttribute("id");
      page.name = std::string(pageElement->Attribute("file"));
      pages.push_back(page);
      pageElement = pageElement->NextSiblingElement("page");
    }

    // Parse Common
    tinyxml2::XMLElement *commonElement = font->FirstChildElement("common");

    Common common = {};
    common.lineHeight = commonElement->IntAttribute("lineHeight");
    common.base = commonElement->IntAttribute("base");
    common.scaleW = commonElement->IntAttribute("scaleW", 1);
    common.scaleH = commonElement->IntAttribute("scaleH", 1);
    common.pages = pages.size();
    common.bitField = commonElement->IntAttribute("bitField");
    common.alphaChnl = commonElement->IntAttribute("alphaChnl");
    common.redChnl = commonElement->IntAttribute("redChnl");
    common.greenChnl = commonElement->IntAttribute("greenChnl");
    common.blueChnl = commonElement->IntAttribute("blueChnl");

    // Parse Chars
    std::vector<Char> chars;
    tinyxml2::XMLElement *charsElement = font->FirstChildElement("chars");
    tinyxml2::XMLElement *charElement = charsElement->FirstChildElement("char");
    while (charElement) {
      Char ch = {};
      ch.id = charElement->IntAttribute("id");
      ch.x = charElement->IntAttribute("x");
      ch.y = charElement->IntAttribute("y");
      ch.width = charElement->IntAttribute("width");
      ch.height = charElement->IntAttribute("height");
      ch.xoffset = charElement->IntAttribute("xoffset");
      ch.yoffset = charElement->IntAttribute("yoffset");
      ch.xadvance = charElement->IntAttribute("xadvance");
      ch.page = charElement->IntAttribute("page");
      ch.chnl = charElement->IntAttribute("chnl");
      chars.push_back(ch);
      charElement = charElement->NextSiblingElement("char");
    }

    // Write Info
    uint32_t fontFaceSize = sizeof(char) * (fontFace.size() + 1);
    uint32_t infoBlockSize = sizeof(info) + fontFaceSize;
    output.write((char*)&BLOCK_TYPE_INFO, 1);
    output.write((char*)&infoBlockSize, 4);
    output.write((char*)&info, sizeof(info));
    output.write(fontFace.c_str(), fontFaceSize);

    // Write Common
    uint32_t commonBlockSize = sizeof(common);
    output.write((char*)&BLOCK_TYPE_COMMON, 1);
    output.write((char*)&commonBlockSize, 4);
    output.write((char*)&common, commonBlockSize);

    // Write Pages
    // Each filename has the same length, so once you know the size of the first name, you can
    // calculate the total size of the block
    uint32_t pageNameLength = sizeof(char) * (pages[0].name.size() + 1);
    uint32_t pagesBlockSize = pageNameLength * pages.size();
    output.write((char*)&BLOCK_TYPE_PAGES, 1);
    output.write((char*)&pagesBlockSize, 4);
    for (int i = 0; i < pages.size(); i++) {
      output.write(pages[i].name.c_str(), pageNameLength);
    }

    // Write Chars
    uint32_t charsBlockSize = sizeof(chars[0]) * chars.size();
    output.write((char*)&BLOCK_TYPE_CHARS, 1);
    output.write((char*)&charsBlockSize, 4);
    for (int i = 0; i < chars.size(); i++) {
      output.write((char*)&chars[i], sizeof(chars[i]));
    }

    output.close();
  }
  return 0;
}
