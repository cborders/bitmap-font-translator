# Font Translator

In tinkering around with an embedded display from [Moddable](https://www.moddable.com/) I found that I wanted more options for fonts than what was included in the examples. I found an open source app for Mac called [FontBuilder](https://github.com/andryblack/fontbuilder) that was able to load in standard font files and create bitmap fonts from them. The Moddable SDK wants the data file to be in a binary format called [BMFont](http://www.angelcode.com/products/bmfont/doc/file_format.html) that wasn't supported by FontBuilder. So I wrote this little translator to convert from the Sparrow format from FontBuilder into the BMFont format for the Moddable SDK.

## Building

```
g++ tinyxml2.cpp font_translator.cpp -o font_translator
```

## Usages

Set up everything in FontBuilder how you like and then export as Sparrow format. This will create a png file and an fnt file. Load the fnt into font_translator and it will convert it to BMF format and overwrite the input file.

```
./font_translator libre_baskerville_bold_14.fnt
```

## Acknowledgements

This app uses [TinyXML2](https://github.com/leethomason/tinyxml2) to load the Sparrow file from FontBuilder.
